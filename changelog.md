# Change Log
All notable changes to this project will be documented in this file.

## [0.0.1.dev0]
### Added
 - dynamic configuration by editing /etc/nginx/nginx.conf (/etc/nginx as volume)
 - nginx server
 - letsencrypt auto generate certificates
 - letsencrypt auto renewal of certificates
 - sample.nginx.conf for reverse proxying
 - sample letsencrypt configuration file (sample.example.com.ini)
