FROM nginx:stable
MAINTAINER Srinivas Devaki (mr.eightnoteight@gmail.com)

VOLUME /data /etc/nginx

RUN awk '$1 ~ "^deb" { $3 = $3 "-backports"; print; exit }' /etc/apt/sources.list > /etc/apt/sources.list.d/backports.list

RUN apt-get update && \
    apt-get upgrade -y
RUN apt-get install bash -y
RUN apt-get install python3 -y

RUN apt-get install certbot -t jessie-backports -y

COPY run.py /data/run.py

ENTRYPOINT ["python3", "/data/run.py"]
