#!/usr/bin/env python3
import glob
import subprocess
import time

def main():
    subprocess.call(['mkdir', '-p', '/data/letsencrypt/configs'])

    # create new certificate with every run
    for site_config in glob.glob('/data/letsencrypt/configs/*.ini'):
        subprocess.call(['certbot', 'certonly', '--config', site_config])

    # run nginx deamon
    subprocess.call(['nginx'])

    while True:
        time.sleep(7 * 24 * 60 * 60)

        # stop nginx
        subprocess.call(['nginx', '-s', 'stop'])
        time.sleep(30)
        subprocess.call(['nginx', '-s', 'quit'])
        time.sleep(30)

        # renew letsencrypt certificates
        subprocess.call(['certbot', 'renew', '--non-interactive'])

        # configuration reload by restarting
        subprocess.call(['nginx'])

main()
